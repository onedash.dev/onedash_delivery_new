import 'package:delivery/blocs/deliveries_bloc.dart';
import 'package:delivery/models/order.dart';
import 'package:delivery/models/theme_model.dart';
import 'package:delivery/services/auth.dart';
import 'package:delivery/services/database.dart';
import 'package:delivery/widgets/cards.dart';
import 'package:delivery/widgets/fade_in.dart';
import 'package:delivery/widgets/texts.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class Deliveries extends StatelessWidget {
  final DeliveriesBloc bloc;

  Deliveries._({@required this.bloc});

  static Widget create(BuildContext context) {
    final auth = Provider.of<AuthBase>(context);
    final database = Provider.of<Database>(context);

    return Provider<DeliveriesBloc>(
      create: (context) => DeliveriesBloc(auth: auth, database: database),
      child: Consumer<DeliveriesBloc>(
        builder: (context, bloc, _) {
          return Deliveries._(bloc: bloc);
        },
      ),
    );
  }

  ScrollController _scrollController = ScrollController();

  int _streamLength = 0;

  @override
  Widget build(BuildContext context) {
    final themeModel = Provider.of<ThemeModel>(context);
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    bool isPortrait =
        MediaQuery.of(context).orientation == Orientation.portrait;

    ///If we reach bottom, add order
    _scrollController.addListener(() {
      if (_scrollController.position.pixels >=
          _scrollController.position.maxScrollExtent - 70) {
        bloc.lengthController.add(_streamLength + 10);
      }
    });

    return StreamBuilder<int>(
      stream: bloc.lengthStream,
      initialData: 15,
      builder: (context, snapshot) {
        _streamLength = snapshot.data;
        return StreamBuilder<List<Order>>(
            stream: bloc.getOrders( _streamLength),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                List<Order> orders = snapshot.data;

                if (snapshot.data.length == 0) {
                  return FadeIn(
                    duration: Duration(milliseconds: 300),
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SvgPicture.asset(
                            'images/nothing_found.svg',
                            width: isPortrait ? width * 0.5 : height * 0.5,
                            fit: BoxFit.cover,
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 30),
                            child: Texts.headline3(
                                'Nothing found!', themeModel.accentColor,
                                alignment: TextAlign.center),
                          )
                        ],
                      ),
                    ),
                  );
                } else {
                  return ListView.builder(
                    physics: AlwaysScrollableScrollPhysics(),
                    controller: _scrollController,
                    itemBuilder: (context, position) {
                      return FadeIn(
                        child: Cards.order(context, order: orders[position]),
                      );
                    },
                    itemCount: orders.length,
                  );
                }
              } else if (snapshot.hasError) {
                return Center(
                  child: SvgPicture.asset(
                    'images/error.svg',
                    width: width * 0.5,
                    fit: BoxFit.cover,
                  ),
                );
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            });
      },
    );
  }
}
