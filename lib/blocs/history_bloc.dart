import 'dart:async';
import 'package:delivery/models/history_item.dart';
import 'package:delivery/services/auth.dart';
import 'package:delivery/services/database.dart';
import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';

class HistoryBloc {
  final Database database;
  final AuthBase auth;

  HistoryBloc({@required this.database, @required this.auth});

  ///Loaded number of history stream
  // ignore: close_sinks
  StreamController<int> lengthController = BehaviorSubject();

  Stream<int> get lengthStream => lengthController.stream;

  ///Get list of orders
  Stream<List<HistoryItem>> getOrders(int length) {
    return database
        .getDataFromCollection("delivery_boys/${auth.email}/history", length)
        .map((snapshots) => snapshots.docs.map((snapshot) {
              return HistoryItem.fromMap(snapshot.data());
            }).toList());
  }
}
