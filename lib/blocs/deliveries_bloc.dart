import 'dart:async';
import 'dart:convert';
import 'package:delivery/helpers/project_configuration.dart';
import 'package:delivery/models/history_item.dart';
import 'package:delivery/models/order.dart';
import 'package:delivery/services/auth.dart';
import 'package:delivery/services/database.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:http/http.dart' as http;

class DeliveriesBloc {
  final AuthBase auth;
  final Database database;

  DeliveriesBloc({@required this.auth, @required this.database});

  //Loaded number of orders stream
  // ignore: close_sinks
  StreamController<int> lengthController = BehaviorSubject();

  Stream<int> get lengthStream => lengthController.stream;

  //Get list of orders
  Stream<List<Order>> getOrders(int length) {
    return database
        .getFromCollectionGroup('orders', length, auth.email, 'date')
        .map((snapshots) => snapshots.docs.map((snapshot) {
              return Order.fromMap(
                  snapshot.data(), snapshot.id, snapshot.reference.path);
            }).toList());
  }

  ///This function is called to confirm order
  Future<void> changeStatus(HistoryItem history, String path) async {
    ///Change order status
    await database.updateData({"status": history.status}, path);

    ///Add history
    await database.setData({
      "order": history.order,
      "status": history.status,
      "date": history.date,
      "image": history.image,
      "comment": history.comment,
    }, "delivery_boys/${auth.email}/history/${path.split("/").last}");

    ///Send Notification to admin and user
    await _sendNotifications("Order status change!",
        "Order nº${path.split("/").last} is ${history.status}", path);
  }

  Future<void> _sendNotifications(
      String title, String content, String orderPath) async {
    ///Send notification to admin
    String adminToken = await _getAdminToken();
    if (adminToken != null) {
      final body = {
        "priority": "high",
        "data": {
          "click_action": "FLUTTER_NOTIFICATION_CLICK",
          "id": 0,
          "status": "done",
          "body": content,
          "title": title
        },
        "to": "$adminToken"
      };

      try {
        await http.post(Uri.parse(ProjectConfiguration.notificationsApi),
            body: json.encode(body));
      } catch (e) {
        print(e);
      }
    }

    ///Send notification to user
    String userId = orderPath.split('/')[1];

    String userToken = await _getUserToken(userId);

    if (userToken != null) {
      final body = {
        "priority": "high",
        "data": {
          "click_action": "FLUTTER_NOTIFICATION_CLICK",
          "id": 0,
          "status": "done",
          "body": content, "title": title
        },
        "to": "$userToken"
      };

      try {
        await http.post(Uri.parse(ProjectConfiguration.notificationsApi),
            body: json.encode(body));
      } catch (e) {
        print(e);
      }
    }
  }

  ///Get user token
  Future<String> _getUserToken(String userId) async {
    print("User Id: " + userId);
    try {
      final snapshot =
          await database.getFutureDataFromDocument("users/$userId");

      print("User Token: " + snapshot.data().toString());
      return snapshot.data()['token'];
    } catch (e) {
      print(e);

      return null;
    }
  }

  ///Get admin token
  Future<String> _getAdminToken() async {
    try {
      final snapshot =
          await database.getFutureDataFromDocument("admin/notifications");
      print("Admin Token: " + snapshot.data()['token']);

      return snapshot.data()['token'];
    } catch (e) {
      return null;
    }
  }
}
